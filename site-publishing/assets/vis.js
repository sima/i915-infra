/*jshint esversion: 6*/

function htmlname(testname) {
    return testname.replace(/[& /|]/, "_")+".html";
}


function buildResultCell(json, key, hrefPrefix) {
    var a = document.createElement("a");

    var [buildname, hostname, testname] = key.split(",");
    var test = json.data[key];

    var result = test ? test.result : "notrun";

    var div = document.createElement("div");
    div.className = result;

    if (result == "timeout")
        div.innerHTML = "T";
    else
        div.innerHTML = "&nbsp;"; // default conent of the cell

    // no tooltips for passes and notruns
    if (result != "pass" && result != "notrun") {
        a.className = "tooltip";

        var b = document.createElement("b");

        b.appendChild(document.createTextNode(buildname + " / " + hostname));
        b.appendChild(document.createElement("br"));
        b.appendChild(document.createTextNode(testname));
        b.appendChild(document.createElement("br"));

        var span = document.createElement("span");
        span.appendChild(b);

        if (test.tooltip && result != "incomplete") {
            var pre = document.createElement("pre");
            pre.innerHTML = test.tooltip;
            span.appendChild(pre);
        }

        div.appendChild(span);
    }

    a.appendChild(div);

    if (test && test.hostname)
        hostname = test.hostname;

    a.href = hrefPrefix + buildname + "/" + hostname + "/" + htmlname(testname);

    return a;
}


function buildHtmlTable(json, reverse, hrefPrefix) {
    var table = document.createElement("table");
    var columns = addAllColumnHeaders(table, json.index.hosts, json.index.builds, json.desc, reverse, hrefPrefix);

    for (var row_id = 0; row_id < json.index.tests.length; row_id++) {
        var tr = document.createElement("tr");
        var test_name_td = document.createElement("td");
        var a = document.createElement("a");

        a.appendChild(document.createTextNode(json.index.tests[row_id]));
        a.href = hrefPrefix + htmlname(json.index.tests[row_id]);

        test_name_td.appendChild(a);
        test_name_td.className="h";

        tr.appendChild(test_name_td);

        for (var column_id = 0; column_id < columns.length; column_id++) {
            var key = columns[column_id] + "," + json.index.tests[row_id];
            var result_td = document.createElement("td");

            if (columns[column_id]) {
                result_td.appendChild(buildResultCell(json, key, hrefPrefix));
                tr.appendChild(result_td);
            } else { // spacer
                result_td.className = "s";
                tr.appendChild(result_td);
            }
        }
        table.appendChild(tr);
    }

    return table;
}


function addAllColumnHeaders(table, index1, index2, desc, reverse, hrefPrefix)
{
    var columns = [];
    var header1, header2, h1postfix, h2postfix;
    var i, imax, th, a;

    if (reverse) {
        header1 = index2;
        header2 = index1;
        h1postfix="/";
        h2postfix=".html";
    } else {
        header1 = index1;
        header2 = index2;
        h1postfix=".html";
        h2postfix="/";
    }

    // level 1 headers with colspan attr and spacers
    // add description if there is one
    var tr = document.createElement("tr");
    tr.appendChild(document.createElement("th"));

    for (i=0, imax = header1.length; i < imax; i++) {
        th = document.createElement("th");
        a = document.createElement("a");
        a.appendChild(document.createTextNode(header1[i]));

        if (desc && desc[header1[i]]) {
            a.appendChild(document.createElement("br"));
            a.appendChild(document.createTextNode(desc[header1[i]]));
        }

        a.href=hrefPrefix+header1[i]+h1postfix;
        th.appendChild(a);
        th.colSpan = header2.length;
        th.setAttribute("class", "g");
        tr.appendChild(th);

        if ( i < imax-1 ) {
            var separator_th = document.createElement("th");
            separator_th.className="s";
            tr.appendChild(separator_th);
        }
    }
    table.appendChild(tr);

    // level 2 headers, collect columnset
    tr = document.createElement("tr");
    tr.appendChild(document.createElement("th"));

    for (i = 0, imax = header1.length; i < imax; i++) {
        for (var j = 0; j < header2.length; j++) {
            var span = document.createElement("span");
            th = document.createElement("th");
            a = document.createElement("a");

            a.appendChild(document.createTextNode(header2[j]));
            a.href=hrefPrefix+header2[j]+h2postfix;

            th.appendChild(a);

            span.className="v";
            span.appendChild(a);

            th.appendChild(span);
            tr.appendChild(th);

            if (reverse) columns.push(header1[i]+","+header2[j]);
            else columns.push(header2[j]+","+header1[i]);
        }
        if ( i < imax-1 ) {
            th = document.createElement("th");
            th.className="s";

            tr.appendChild(th);

            columns.push(null);
        }
    }
    table.appendChild(tr);

    return columns;
}


function readBlob(file, callback, callback_fail) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, true);
    rawFile.responseType = "arraybuffer";
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === XMLHttpRequest.DONE && rawFile.status == "200") {
            callback(rawFile.response);
        } else if (rawFile.status == "404") {
            callback_fail();
        }
    };
    rawFile.send(null);
}


/*jshint bitwise: false*/
function arrayToUTF8(data) {
    var str = "";
    var charCode, charFamily, i;

    for (i=0; i < data.length; i++) {
        charFamily = data[i] >> 4;
        charCode = 0;

        if (!(charFamily & 0b1000)) {
            charCode = data[i];
        } else if ((charFamily & 0b1110) == 0b1100) {
            charCode = ((data[i] & 0x1F) << 6) | (data[i+1] & 0x3F);
            i++;
        } else if ((charFamily & 0b1111) == 0b1110) {
            charCode = ((data[i] & 0x0F) << 12) | ((data[i+1] & 0x3F) << 6) | (data[i+2] & 0x3F);
            i += 2;
        } else {
            continue; // broken char!
        }

        str += String.fromCharCode(charCode);
    }

    return str;
}
/*jshint bitwise: true*/


function drawResults(file, target_div, spinner_div, group_by_run, depth) {
    var hrefPrefix = "";

    if (typeof depth != "undefined")
        hrefPrefix = new Array(depth + 1).join("../");

    readBlob(file, function(blob){
        var json = JSON.parse(arrayToUTF8(pako.inflate(blob)));

        document.title += " " + json.name;
        document.title += " (" + file.split(".")[0].toUpperCase() + ")";

        var e = document.getElementById(spinner_div);
        e.parentNode.removeChild(e);
        document.getElementById(target_div).appendChild(buildHtmlTable(json, group_by_run, hrefPrefix));
    }, function(){
        var e = document.getElementById(spinner_div);

        if (e)
            e.parentNode.removeChild(e);

        document.getElementById(target_div).innerText="no results (yet?)";
    });
}
